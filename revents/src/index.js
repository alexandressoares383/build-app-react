import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css'
import App from './layout/App';
import { BrowserRouter } from 'react-router-dom';

const rootEl = document.getElementById('root');

let render = () => {
    ReactDOM.render(
        <BrowserRouter>
            <App />
        </BrowserRouter>, 
        rootEl
    )
}

if(module.hot) {
    module.hot.accept('./layout/App', () => {
        setTimeout(render);
    })
}

render();