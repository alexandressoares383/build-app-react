import React, { Component, Fragment } from 'react';
import { Container } from 'semantic-ui-react';

/** Components */
import NavBar from '../features/nav/NavBar/NavBar';
import { Route } from 'react-router-dom';
import HomePage from '../features/home/HomePage';
import EventDashboard from '../features/event/EventDashboard/EventDashboard';
import EventDetailedPage from '../features/event/EventDetailed/EventDetailedPage';
import PeopleDashboard from '../features/user/PeopleDashboard/PeopleDashboard';
import UserDetailedPage from '../features/userDetailed/UserDetailedPage';
import SettingsDashboard from '../features/user/Settings/SettingsDashboard';
import EventForm from '../features/event/EventFom/EventForm';

class App extends Component {
  render() {
    return (
      <Fragment>
        <NavBar />
        <Container className="main">
          <Route path="/" component={HomePage} />
          <Route path="/events" component={EventDashboard} />
          <Route path="/events/:id" component={EventDetailedPage} />
          <Route path="/people" component={PeopleDashboard} />
          <Route path="/profile/:id" component={UserDetailedPage} />
          <Route path="/settings" component={SettingsDashboard} />
          <Route path="/createEvent" component={EventForm} />
        </Container>
      </Fragment>
    );
  }
}

export default App;
